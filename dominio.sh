#!/bin/sh
echo 'Direccion del dominio:'
read dom
echo 'Server:'
read srv
sudo touch /etc/nginx/sites-available/$dom.conf
printf 'server {\n' >> /etc/nginx/sites-available/$dom.conf
printf '  listen 80;\n' >> /etc/nginx/sites-available/$dom.conf
printf '  server_name' >> /etc/nginx/sites-available/$dom.conf
printf ''\ $dom'' >> /etc/nginx/sites-available/$dom.conf
printf ';\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '  include snippets/letsencrypt.conf;\n' >> /etc/nginx/sites-available/$dom.conf
printf '}' >> /etc/nginx/sites-available/$dom.conf
sudo ln -s /etc/nginx/sites-available/$dom.conf /etc/nginx/sites-enabled/$dom.conf
sudo systemctl reload nginx
sudo certbot certonly --agree-tos --email felipe@pudutechnology.cl --webroot -w /var/lib/letsencrypt/ -d $dom
echo 'ACTUALIZANDO BLOQUE'
sed -i '1,$d' /etc/nginx/sites-available/$dom.conf
printf 'upstream' >> /etc/nginx/sites-available/$dom.conf
printf ''\ $srv'' >> /etc/nginx/sites-available/$dom.conf
printf '{\n' >> /etc/nginx/sites-available/$dom.conf
printf '    server 127.0.0.1:8069;\n' >> /etc/nginx/sites-available/$dom.conf
printf '}\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf 'server {\n' >> /etc/nginx/sites-available/$dom.conf
printf '    listen 80;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    server_name' >> /etc/nginx/sites-available/$dom.conf
printf ''\ $dom'' >> /etc/nginx/sites-available/$dom.conf
printf ';\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '    include snippets/letsencrypt.conf;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    return 301 https://$host$request_uri;\n' >> /etc/nginx/sites-available/$dom.conf
printf '}\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf 'server {\n' >> /etc/nginx/sites-available/$dom.conf
printf '    listen 443 ssl http2;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    server_name' >> /etc/nginx/sites-available/$dom.conf
printf ''\ $dom'' >> /etc/nginx/sites-available/$dom.conf
printf ';\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '    ssl_certificate /etc/letsencrypt/live' >> /etc/nginx/sites-available/$dom.conf
printf ''\/$dom'' >> /etc/nginx/sites-available/$dom.conf
printf '/fullchain.pem;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    ssl_certificate_key /etc/letsencrypt/live' >> /etc/nginx/sites-available/$dom.conf
printf ''\/$dom'' >> /etc/nginx/sites-available/$dom.conf
printf '/privkey.pem;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    ssl_trusted_certificate /etc/letsencrypt/live' >> /etc/nginx/sites-available/$dom.conf
printf ''\/$dom'' >> /etc/nginx/sites-available/$dom.conf
printf '/chain.pem;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    include snippets/ssl.conf;\n' >> /etc/nginx/sites-available/$dom.conf
printf '    include snippets/letsencrypt.conf;\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '    location / {\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_next_upstream error timeout invalid_header http_500 http_502  http_503 http_504;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_redirect off;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_set_header Host $host;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_set_header X-Real-IP $remote_addr;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_set_header X-Forwarded-Proto https;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_pass http:/' >> /etc/nginx/sites-available/$dom.conf
printf ''\/$srv'' >> /etc/nginx/sites-available/$dom.conf
printf ';\n' >> /etc/nginx/sites-available/$dom.conf
printf '        }\n' >> /etc/nginx/sites-available/$dom.conf
printf '        location ~* /web/static/ {\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_cache_valid 200 60m;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_buffering on;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                expires 864000;\n' >> /etc/nginx/sites-available/$dom.conf
printf '                proxy_pass http:/' >> /etc/nginx/sites-available/$dom.conf
printf ''\/$srv'' >> /etc/nginx/sites-available/$dom.conf
printf ';\n' >> /etc/nginx/sites-available/$dom.conf
printf '        }\n' >> /etc/nginx/sites-available/$dom.conf
printf '\n' >> /etc/nginx/sites-available/$dom.conf
printf '}\n' >> /etc/nginx/sites-available/$dom.conf
sudo systemctl reload nginx
sed -i '1,$d' /etc/cron.d/certbot
printf '# /etc/cron.d/certbot: crontab entries for the certbot package\n' >> /etc/cron.d/certbot
printf '#\n' >> /etc/cron.d/certbot
printf '# Upstream recommends attempting renewal twice a day\n' >> /etc/cron.d/certbot
printf '#\n' >> /etc/cron.d/certbot
printf '# Eventually, this will be an opportunity to validate certificates\n' >> /etc/cron.d/certbot
printf '# havent' >> /etc/cron.d/certbot
printf ''\''''' >> /etc/cron.d/certbot
printf 't been revoked, etc.  Renewal will only occur if expiration\n' >> /etc/cron.d/certbot
printf '# is within 30 days.\n' >> /etc/cron.d/certbot
printf 'SHELL=/bin/sh\n' >> /etc/cron.d/certbot
printf 'PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin\n' >> /etc/cron.d/certbot
printf '0 */12 * * * root test -x /usr/bin/certbot -a \! -d /run/systemd/system && perl -e ' >> /etc/cron.d/certbot
printf ''\''sleep int(rand(43200))'\''' >> /etc/cron.d/certbot
printf ' && certbot -q renew --renew-hook ' >> /etc/cron.d/certbot
printf '"systemctl reload nginx"\n' >> /etc/cron.d/certbot
printf '\n' >> /etc/cron.d/certbot
sudo certbot renew --dry-run