echo -e "ACTUALIZANDO UBUNTU" 
sudo apt update && sudo apt upgrade -y
clear
echo -e "INSTALANDO NGINX"
sleep 3s
sudo apt update
sudo apt install nginx -y
sudo systemctl status nginx
echo -e "INSTALANDO CERBOT"
sudo apt update && sudo apt upgrade -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:certbot/certbot
sudo apt update && sudo apt upgrade -y
sudo apt install certbot -y
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
clear
echo -e "OBTENIENDO CERTIFICADO SSL"
mkdir -p /var/lib/letsencrypt/.well-known
chgrp www-data /var/lib/letsencrypt
chmod g+s /var/lib/letsencrypt
echo -e "CREANDO SNIPPETS NGINX"
sudo touch /etc/nginx/snippets/letsencrypt.conf
printf 'location ^~ /.well-known/acme-challenge/ {\n' >> /etc/nginx/snippets/letsencrypt.conf
printf '  allow all;\n' >> /etc/nginx/snippets/letsencrypt.conf
printf '  root /var/lib/letsencrypt/;\n' >> /etc/nginx/snippets/letsencrypt.conf
printf '  default_type "text/plain";\n' >> /etc/nginx/snippets/letsencrypt.conf
printf '  try_files $uri =404;\n' >> /etc/nginx/snippets/letsencrypt.conf
printf '}' >> /etc/nginx/snippets/letsencrypt.conf
sudo touch /etc/nginx/snippets/ssl.conf
printf 'ssl_dhparam /etc/ssl/certs/dhparam.pem;\n' >> /etc/nginx/snippets/ssl.conf
printf '\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_session_timeout 1d;\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_session_cache shared:SSL:50m;\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_session_tickets off;\n' >> /etc/nginx/snippets/ssl.conf
printf '\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_protocols TLSv1 TLSv1.1 TLSv1.2;\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_ciphers ' >> /etc/nginx/snippets/ssl.conf
printf ''\''ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS'\'';\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_prefer_server_ciphers on;\n' >> /etc/nginx/snippets/ssl.conf
printf '\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_stapling on;\n' >> /etc/nginx/snippets/ssl.conf
printf 'ssl_stapling_verify on;\n' >> /etc/nginx/snippets/ssl.conf
printf 'resolver 8.8.8.8 8.8.4.4 valid=300s;\n' >> /etc/nginx/snippets/ssl.conf
printf 'resolver_timeout 30s;\n' >> /etc/nginx/snippets/ssl.conf
printf '\n' >> /etc/nginx/snippets/ssl.conf
printf 'add_header Strict-Transport-Security ' >> /etc/nginx/snippets/ssl.conf
printf '"max-age=15768000; includeSubdomains; preload";\n' >> /etc/nginx/snippets/ssl.conf
printf 'add_header X-Frame-Options SAMEORIGIN;\n' >> /etc/nginx/snippets/ssl.conf
printf 'add_header X-Content-Type-Options nosniff;\n' >> /etc/nginx/snippets/ssl.conf
